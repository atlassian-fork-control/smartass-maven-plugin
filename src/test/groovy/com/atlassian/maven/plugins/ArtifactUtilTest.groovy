/*
 * Copyright © 2015 Atlassian Pty Ltd. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.maven.plugins

import com.atlassian.maven.plugins.ArtifactUtil
import org.apache.maven.artifact.Artifact
import org.apache.maven.artifact.factory.ArtifactFactory
import org.apache.maven.plugin.MojoExecutionException
import org.hamcrest.CoreMatchers
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.ExpectedException
import org.junit.rules.MethodRule
import org.mockito.Mock
import org.mockito.junit.MockitoJUnit

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertSame
import static org.mockito.Mockito.*

class ArtifactUtilTest {

    @Rule
    public MethodRule initMocks = MockitoJUnit.rule();
    @Rule
    public ExpectedException exception = ExpectedException.none()

    @Mock
    ArtifactFactory artifactFactory;

    private ArtifactUtil artifactUtil;

    @Before
    public void setUp() {
        artifactUtil = new ArtifactUtil(artifactFactory)
    }

    @Test
    public void artifactToStringConversionNoClassifier() throws Exception {
        mockArtifact { Artifact artifact ->
            assertEquals('g:a:v:p', artifactUtil.toString(artifact))
            verify(artifact).getClassifier();
        }
    }

    @Test
    public void artifactToStringConversionWithClassifier() throws Exception {
        mockArtifact { Artifact artifact ->
            when(artifact.getClassifier()).thenReturn('c')
            assertEquals('g:a:v:p:c', artifactUtil.toString(artifact))
        }
    }

    @Test
    public void stringToArtifactConversionNoClassifier() throws Exception {
        mockArtifact { Artifact artifact ->
            when(artifactFactory.createArtifact('g', 'a', 'v', null, 'p')).thenReturn(artifact)
            assertSame(artifact, artifactUtil.fromString('g:a:v:p'))
        }
    }

    @Test
    public void stringToArtifactConversionWithClassifier() throws Exception {
        mockArtifact { Artifact artifact ->
            when(artifactFactory.createArtifactWithClassifier('g', 'a', 'v', 'p', 'c')).thenReturn(artifact)
            assertSame(artifact, artifactUtil.fromString('g:a:v:p:c'))
        }
    }

    @Test
    public void mojoExecutionIsThrownOnParseErrorsNotEnoughTokens() throws Exception {
        def artifactString = 'g:a:v'

        exception.expect(MojoExecutionException.class)
        exception.expectMessage(CoreMatchers.containsString("'${artifactString}'".toString()))

        artifactUtil.fromString(artifactString)
    }

    @Test
    public void mojoExecutionIsThrownOnParseErrorsTooManyTokens() throws Exception {
        def artifactString = 'g:a:v:p:c:woot'

        exception.expect(MojoExecutionException.class)
        exception.expectMessage(CoreMatchers.containsString("'${artifactString}'".toString()))

        artifactUtil.fromString(artifactString)
    }

    private static void mockArtifact(Closure testWithMock) {

        Artifact mock = mock(Artifact.class)

        when(mock.getGroupId()).thenReturn('g')
        when(mock.getArtifactId()).thenReturn('a')
        when(mock.getVersion()).thenReturn('v')
        when(mock.getType()).thenReturn('p')

        testWithMock(mock)
    }
}
